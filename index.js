document.addEventListener("DOMContentLoaded", function () {
    const apiKey = "035c9bcae77c4a72b46d1f71044fcbed";
    const newsContainer = document.getElementById("news-container");
  
    fetch(`https://newsapi.org/v2/top-headlines?country=us&apiKey=${apiKey}`)
      .then((response) => response.json())
      .then((data) => {
        const articles = data.articles;
        articles.forEach((article) => {
          const card = document.createElement("div");
          card.classList.add("card");
  
          const title = document.createElement("h2");
          title.textContent = article.title;
  
          const image = document.createElement("img");
          image.src = article.urlToImage ? article.urlToImage : "placeholder.png";
          image.alt = "Article Image";
  
          const description = document.createElement("p");
          description.textContent = article.description ? article.description : "No description available.";
  
          const source = document.createElement("p");
          source.textContent = `Source: ${article.source.name}`;
  
          const link = document.createElement("a");
          link.href = article.url;
          link.textContent = "Read more";
          link.target = "_blank";
  
          card.appendChild(title);
          card.appendChild(image);
          card.appendChild(description);
          card.appendChild(source);
          card.appendChild(link);
  
          newsContainer.appendChild(card);
        });
      })
      .catch((error) => {
        console.error("Error fetching news:", error);
      });
  });